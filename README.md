# Demo for providing instant loans using blockchain! 
###### Version 0.0.1
In a network of three parties where there is a e-commerce site, a vendor and a bank, this project allows the bank to get access to the invoices of the vendors which are due by the ecommerce platform and provide them with loans. 

## Dependencies

The project has been built by using the hyperledger fabric and fabric composer was used for quick prototyping for the hackathon. <br>
You will need the following installed: <br>
- Docker
- Hyperledger fabric
- Hyperledger composer
<br>
Url's and other references will be added further
  
## Files
The project contains the source code of the platform built using fabric composer but also contains the **.bna** file which can be used to run the project in a test environment, **composer playground** is a perfect candidate for this. 

> Both the online version or a local version can be used to run it, the online version is easier to use since the dependencies are not needed.
 
## How to run the project

> This part would be updated later. 
