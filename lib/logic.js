'use strict';
/**
 * Listing a product on a site
 * @param {org.mycompany.hacknet.listProduct} args 
 * @transaction
 */

function listProduct(args) {
	args.commodity.listed = args.ecommerce ;
	
  return getAssetRegistry('org.mycompany.hacknet.Commodity')
        .then(function (assetRegistry) {
            return assetRegistry.update(args.commodity);
        }) ;
}


/**
 * Selling a product and adding it to the invoice
 * @param {org.mycompany.hacknet.productSold} args 
 * @transaction
 */

function productsSold(args) {
    args.commodity.sold = true ;
  	var vendor = args.commodity.seller ;
  	var currentInvoice = vendor.currentInvoice ;
  	var sellingPrice = args.commodity.price ;
  	
  	currentInvoice.amountDue += sellingPrice ;
  	
  	return getAssetRegistry('org.mycompany.hacknet.Commodity')
        .then(function (assetRegistry) {
            return assetRegistry.update(args.commodity) ;
        })
  		.then(function() {
      		return getAssetRegistry('org.mycompany.hacknet.Invoice')
      		.then( function (assetRegistry) {
            	return assetRegistry.update(currentInvoice) ;
            })
    	}) ;
}



/**
 * Selling a product and adding it to the invoice
 * @param {org.mycompany.hacknet.loanGranting} args 
 * @transaction
 */

function loanGranting(args) {
  	//Vendor type
  	var loanApplicant = args.loanApp.applicant ;
  	
  	if(loanApplicant.currentInvoice.amountDue > args.invoiceThresholdValue) {
    	args.loanApp.granted = true ;
    	args.loanApp.disbursed = false ;
      	
      	return getAssetRegistry('org.mycompany.hacknet.LoanApplication')
        .then(function (assetRegistry) {
            return assetRegistry.update(args.loanApp) ;
        }) ;
    }
  	else{
    	throw new Error('Too risky, loan not granted') ;
    }
}