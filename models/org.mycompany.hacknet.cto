/**
 * Write your model definitions here
 */

namespace org.mycompany.hacknet

participant Customer identified by UserID{
	o String UserID
}

participant Vendor identified by CompanyID {
	o String CompanyID
    o String CompanyName
    --> Invoice currentInvoice optional
}

participant EcommerceSite identified by CompanyID {
	o String CompanyID
    o String CompanyName
}

participant Bank identified by CompanyID{
	o String CompanyID   
}

asset Commodity identified by ProductID {
	o String ProductID
  	o Double price
    o Double productQuantity
  	o Boolean sold
    --> Vendor seller
    --> EcommerceSite listed optional
}

asset Invoice identified by InvoiceId {
	o String InvoiceId
    o String VendorId
    o String EcommerceId
    o Double amountDue
    o String dateDue
    o String[] productsSold
}

asset LoanApplication identified by ApplicationId {
	o String ApplicationId
    o Boolean granted
    o Boolean disbursed
    o Double amountRequested
    --> Vendor applicant
}

transaction listProduct {
	--> EcommerceSite ecommerce
    --> Commodity commodity
}

transaction productSold {
	--> Commodity commodity
}

transaction loanGranting {
    o Double invoiceThresholdValue
	--> LoanApplication loanApp  
}
